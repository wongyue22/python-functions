from ast import arg
import numbers


def add(*numbers):
    total = 0
    for num in numbers:
        total += num
    print(numbers)
    return total


total = add(1, 2, 3, 4)
print(total)
print(add())  # Expect 0
print(add(2))  # Expect 2
print(add(3, 9, 100, 7))  # Expect 110


def double_splat(**mystery):
    print(mystery)


double_splat()
double_splat(name="Noor")
double_splat(first_name="Baz", last_name="Sayid")
